# Opening Sign: shifts to schedule library

## Purpose

This library has the means to be used by the Opening Sign project, and is here to convert shift rules inputted by the user into data for a weekly schedule.

## Installation

`npm install opening-sign-shifts-to-schedule`

## Usage
Feed it shift rules! It outputs a weekly schedule.
```js
schedule(rules);
```
### *Schedule()* function

#### Input

```js
rules: [
  {
    // Every week, from Monday to Thursday, the opening times are from 8:00 to 18:00
    repeating: "weekly",
    repeatingDates: ["mon", "tue", "wed", "thu"],
    isClosed: false,
    shift: [
      28800, // 08:00, stored as seconds past midnight
      64800, // 18:00
    ],
  },
  {
    // On 06/22/2021, the opening times are from 20:30 to 03:00 (night shift)
    repeating: "exceptional",
    repeatingDates: [
      1624320000, // Epoch timestamp (seconds) for 06/22/2021 at 00:00 (time is not relevant, only date matters)
      // etc...
    ],
    isClosed: false,
    shift: [
      // a pair of seconds past midnight on the RepeatingDates
      73800, // 20:30, stored as seconds past midnight
      97200, // 27:00 (24:00 + 03:00), the next day. As this is over 1440 minutes (=24h) it will be detected as a night shift
    ],
  },
  {
    // Every Monday, it is closed from 12:00 to 14:00
    repeating: "weekly",
    repeatingDates: ["mon"],
    isClosed: true,
    shift: [
      43200, // 12:00 closes
      50400, // 14:00 opens again
    ],
  },
];
```

The `shift` is to be stored as a pair, it will define the start and the end of the shift (reversed when `isClosed: true`)

#### Output

```js
schedule: {
	mon: [
		28800, // 08:00 stored as seconds past midnight
		43200, // 12:00 closes
		50400, // 14:00 opens again
		64800  // 18:00
	],
	tue: [
		28800, // 08:00, stored as seconds past midnight
		64800  // 18:00
	],
	wed: [
		28800, // 08:00, stored as seconds past midnight
		64800  // 18:00
	],
	thu: [
		28800, // 08:00, stored as seconds past midnight
		64800  // 18:00
	],
	exceptional: [
		{
			date: 1624320000, // On 06/22/2021
			overrideRegular: false, // here, it will follow regular breaks overlapping with it (if break from 00:00 to 2:00, will write 20:30-00:00-2:00-3:00)
			opening: [
				73800,   // 20:30, stored as seconds past midnight
				97200    // 27:00 (24:00 + 03:00), the next day. As this is over 1440 minutes (=24h) it will be detected as a night shift
			]
		}
	]
}
```

### Roadmap

- [ ] exceptional shifts support
- [ ] merging overlapping shifts
- [x] unit tests
- [x] day shifts support
- [x] closing shifts support
- [x] subtracting closing shifts to opening shifts
- [x] night shifts support

### Changelog
#### 1.1.x - refactoring files
- each side function now has its own file
- tests are now in multiple files
- weekDays is exported so you can use it in your project (it's an array of 3 letters weekdays)
- every function is exported too
#### 1.0.x - Initial version!
- Working weekly shifts.
- Working night shifts.
- Working closing shifts.

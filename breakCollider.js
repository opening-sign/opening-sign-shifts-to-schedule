/**
 * subtract a closing shift to an opening one
 * @param {[number, number]} o - the opening shift (pair of seconds past midnight)
 * @param {[number, number]} c - the closing shift (pair of seconds past midnight)
 * @returns {[]|[number, number]|[number, number, number, number]}
 */

export function breakCollider(o, c) {
  //           0oooooooooooooooooooo1
  //+  0cccccccccccccccccccccccccccccccccc1
  //=           -nothing-
  if (c[0]<=o[0] && o[0]<o[1] && o[1]<=c[1]) {
    return [];
  }
  //           0ooooooooooooooooooooooooo1
  //+  0cccccccccccccccccc1
  //=                     0oooooooooooooo1
  if (c[0]<=o[0] && o[0]<c[1] && c[1]<o[1]) {
    return [c[1], o[1]];
  }

  //  0oooooooooooooooooooo1
  //+       0cccccccccccccccccccccccccccc1
  //= 0ooooo1
  if (o[0]<c[0] && c[0]<o[1] && o[1]<=c[1]) {
    return [o[0], c[0]];
  }

  //   0ooooooooooooooooooooooooooooooooo1
  //+       0cccccccccccccc1
  //=  0oooo1              2ooooooooooooo3
  if (o[0]<c[0] && c[0]<c[1] && c[1]<=o[1]) {
    return [o[0], c[0], c[1], o[1]];
  }

  // if out of bounds, return the original opening shift
  return [o[0], o[1]];
}

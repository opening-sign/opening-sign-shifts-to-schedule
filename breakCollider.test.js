import { breakCollider } from './breakCollider.js';

test("out of bounds", () => {
	expect(breakCollider([0, 1], [2, 3]))
		.toStrictEqual([0, 1]);
});
test("colliding in the middle", () => {
	expect(breakCollider([1, 4], [2, 3]))
		.toStrictEqual([1, 2, 3, 4]);
});
test("colliding in the left", () => {
	expect(breakCollider([8, 15], [12, 18]))
		.toStrictEqual([8, 12]);
});
test("colliding in the right", () => {
	expect(breakCollider([1, 3], [2, 4]))
		.toStrictEqual([1, 2]);
});
test("cancelling the openings", () => {
	expect(breakCollider([1, 2], [1, 2]))
		.toStrictEqual([]);
});
test("wrapping the openings", () => {
	expect(breakCollider([2, 3], [1, 4]))
		.toStrictEqual([]);
});

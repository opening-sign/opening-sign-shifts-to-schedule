import { breakCollider } from "./breakCollider.js";
import { nightShiftSlicer } from "./nightShiftSlicer.js";

export const weekDays = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];

/**
 * Outputs a weekly schedule from shift rules
 * @param {Object[]} rules - an array of objects storing each rule (likely inputted by the user).
 * @param {string} rules[].repeating - which type of repetition the current rule is using.
 * @param {(string[]|number[])} rules[].repeatingDates - either week days or Epoch timestamps (seconds).
 * @param {bool} rules[].isClosed - is it a closing rule?
 * @param {number[]} rules[].shift - a pair of seconds past midnight
 */
export function schedule(rules) {
  let output = {
    mon: [],
    tue: [],
    wed: [],
    thu: [],
    fri: [],
    sat: [],
    sun: [],
    exceptional: [],
  };

  // 1. sanitize rules
  let separatedRules = rules.reduce((accumulator, rule) => {
      // 1.1. manage night shifts
      rule = nightShiftSlicer(rule);

      // 1.2. Separate closing rules from opening rules
      if (rule.isClosed) {
        return {
          opening: accumulator.opening,
          closing: accumulator.closing.concat(rule),
        };
      } else {
        return {
          opening: accumulator.opening.concat(rule),
          closing: accumulator.closing,
        };
      }
    }, {opening: [],closing: []});

  // 2. populate the schedule day by day
  weekDays.forEach((day) => {
    // 2.1. inject openings
    output[day] = separatedRules.opening.reduce((dayAccumulator, rule) => {
      if (rule.repeatingDates.includes(day)) {
        return dayAccumulator.concat(rule.shift).sort((a, b) => a - b);
      }
      return dayAccumulator;
    }, []);

    // 2.2. inject closings
    separatedRules.closing.forEach((closingRule) => {
      if (closingRule.repeatingDates.includes(day)) {
        // For each shift of this day
        for (let index = 0; index + 1 < output[day].length; index += 2) {
          const openingShift = [output[day][index], output[day][index + 1]];
          output[day].splice(index, 2, ...breakCollider(openingShift, closingRule.shift));
        }
      }
    }, []);
  });

  return output;
}

export { breakCollider, nightShiftSlicer };

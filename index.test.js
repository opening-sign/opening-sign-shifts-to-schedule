import { schedule } from "./index.js";

// describe("exceptions", () => {
//   test(`2021-06-22 & 2021-07-22, open 20:30-03:00. 2021-06-23, open 08:00-09:00.`, () => {
//     expect(
//       schedule([
//         {
//           repeating: "exceptional",
//           repeatingDates: [1624320000, 1626912000],
//           isClosed: false,
//           shift: [73800, 97200],
//         },
//         {
//           repeating: "exceptional",
//           repeatingDates: [1624406400],
//           isClosed: false,
//           shift: [28800, 32400],
//         },
//       ])
//     ).toStrictEqual({
//       mon: [],
//       tue: [],
//       wed: [],
//       thu: [],
//       fri: [],
//       sat: [],
//       sun: [],
//       exceptional: [
//         {
//           date: 1624320000,
//           shift: [73800, 97200],
//         },
//         {
//           date: 1626912000,
//           shift: [73800, 97200],
//         },
//         {
//           date: 1624406400,
//           shift: [28800, 32400],
//         },
//       ],
//     });
//   });
// });

describe("openings", () => {
  test("Mon, open 8:00-18:00.", () => {
    expect(
      schedule([
        {
          repeating: "weekly",
          repeatingDates: ["mon"],
          isClosed: false,
          shift: [28800, 64800],
        },
      ])
    ).toStrictEqual({
      mon: [28800, 64800],
      tue: [],
      wed: [],
      thu: [],
      fri: [],
      sat: [],
      sun: [],
      exceptional: [],
    });
  });

  test("Every day, open 8:00-18:00.", () => {
    expect(
      schedule([
        {
          repeating: "weekly",
          repeatingDates: ["mon", "tue", "wed", "thu", "fri", "sat", "sun"],
          isClosed: false,
          shift: [28800, 64800],
        },
      ])
    ).toStrictEqual({
      mon: [28800, 64800],
      tue: [28800, 64800],
      wed: [28800, 64800],
      thu: [28800, 64800],
      fri: [28800, 64800],
      sat: [28800, 64800],
      sun: [28800, 64800],
      exceptional: [],
    });
  });

  test("Mon, open 18:00-02:00 (night shift).", () => {
    expect(
      schedule([
        {
          repeating: "weekly",
          repeatingDates: ["mon"],
          isClosed: false,
          shift: [64800, 93600],
        }
      ])
    )
      .toStrictEqual({
        mon: [64800, 86400],
        tue: [0, 7200],
        wed: [],
        thu: [],
        fri: [],
        sat: [],
        sun: [],
        exceptional: [],
      });
  });

  test("Mon to Thu, open 8:00-18:00.", () => {
    expect(
      schedule([
        {
          repeating: "weekly",
          repeatingDates: ["mon", "tue", "wed", "thu"],
          isClosed: false,
          shift: [28800, 64800],
        },
      ])
    ).toStrictEqual({
      mon: [28800, 64800],
      tue: [28800, 64800],
      wed: [28800, 64800],
      thu: [28800, 64800],
      fri: [],
      sat: [],
      sun: [],
      exceptional: [],
    });
  });
});

describe("openings & closings", () => {
  test(`Mon, open 8:00-18:00. Mon, close 12:00-14:00.`, () => {
    expect(
      schedule([
        {
          repeating: "weekly",
          repeatingDates: ["mon"],
          isClosed: false,
          shift: [28800, 64800],
        },
        {
          repeating: "weekly",
          repeatingDates: ["mon"],
          isClosed: true,
          shift: [43200, 50400],
        },
      ])
    ).toStrictEqual({
      mon: [28800, 43200, 50400, 64800],
      tue: [],
      wed: [],
      thu: [],
      fri: [],
      sat: [],
      sun: [],
      exceptional: [],
    });
  });

  test("Mon to Thu, open 8:00-18:00. Mon to Tue, close 12:00-14:00.", () => {
    expect(
      schedule([
        {
          repeating: "weekly",
          repeatingDates: ["mon", "tue", "wed", "thu"],
          isClosed: false,
          shift: [28800, 64800],
        },
        {
          repeating: "weekly",
          repeatingDates: ["mon", "tue"],
          isClosed: true,
          shift: [43200, 50400],
        },
      ])
    ).toStrictEqual({
      mon: [28800, 43200, 50400, 64800],
      tue: [28800, 43200, 50400, 64800],
      wed: [28800, 64800],
      thu: [28800, 64800],
      fri: [],
      sat: [],
      sun: [],
      exceptional: [],
    });
  });

  test("Mon, open 18:00-02:00 (night shift).", () => {
    expect(
      schedule([
        {
          repeating: "weekly",
          repeatingDates: ["mon"],
          isClosed: false,
          shift: [64800, 93600],
        },
        {
          repeating: "weekly",
          repeatingDates: ["tue"],
          isClosed: true,
          shift: [0, 3600],
        }
      ])
    )
      .toStrictEqual({
        mon: [64800, 86400],
        tue: [3600, 7200],
        wed: [],
        thu: [],
        fri: [],
        sat: [],
        sun: [],
        exceptional: [],
      });
  });
});

describe("overlapping", () => {
  test("Mon, open 8:00-15:00. Mon, open 12:00-18:00.", () => {
    expect(
      schedule([
        {
          repeating: "weekly",
          repeatingDates: ["mon", "tue"],
          isClosed: false,
          shift: [8, 15],
        },
        {
          repeating: "weekly",
          repeatingDates: ["mon"],
          isClosed: true,
          shift: [12, 18],
        },
      ])
    ).toStrictEqual({
      mon: [8, 12],
      tue: [8, 15],
      wed: [],
      thu: [],
      fri: [],
      sat: [],
      sun: [],
      exceptional: [],
    });
  });
});

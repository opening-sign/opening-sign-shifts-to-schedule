import { weekDays } from "./index.js";

/**
 * 
 * @param {Object} rule - the rule to slice to the next day if it goes past midnight
 * @param {number} [oneDay=60*60*24] - seconds * minutes * hours in one day
 * @returns 
 */
export function nightShiftSlicer(rule, oneDay = 60*60*24) {
  if (rule.shift[1] < oneDay) {
    return rule;
  }
  // Deep copy the day to a new rule, then make it start at 00:00 on the next day
  let newRule = JSON.parse(JSON.stringify(rule));
  newRule.shift[0] = 0;
  newRule.shift[1] -= oneDay;
  rule.shift[1] = oneDay;

  // shift the newRule day to the next day (mon -> tue, sun -> mon, etc.)
  newRule.repeatingDates = newRule.repeatingDates.map((date) => {
    let dayNumber = weekDays.indexOf(date) + 1;
    while (dayNumber > weekDays.length - 1) {
      dayNumber -= weekDays.length;
    }
    return weekDays[dayNumber];
  });
  return [rule, newRule];
}

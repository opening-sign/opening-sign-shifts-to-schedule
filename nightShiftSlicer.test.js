import { nightShiftSlicer } from "./nightShiftSlicer.js";

test("Mon, open 18:00-02:00", () => {
	expect(
		nightShiftSlicer({
			repeating: "weekly",
			repeatingDates: ["mon"],
			isClosed: false,
			shift: [64800, 93600],
		})
	)
		.toStrictEqual([
			{
				repeating: "weekly",
				repeatingDates: ["mon"],
				isClosed: false,
				shift: [64800, 86400],
			},
			{
				repeating: "weekly",
				repeatingDates: ["tue"],
				isClosed: false,
				shift: [0, 7200],
			}
		]);
});

test("Sun, open 18:00-02:00", () => {
	expect(
		nightShiftSlicer({
			repeating: "weekly",
			repeatingDates: ["sun"],
			isClosed: false,
			shift: [64800, 93600],
		})
	)
		.toStrictEqual([
			{
				repeating: "weekly",
				repeatingDates: ["sun"],
				isClosed: false,
				shift: [64800, 86400],
			},
			{
				repeating: "weekly",
				repeatingDates: ["mon"],
				isClosed: false,
				shift: [0, 7200],
			}
		]);
});
